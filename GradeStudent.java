/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



/**
 *
 * @author ngocb
 */
public class GradeStudent {
    
    static Scanner input = new Scanner(System.in);
    static int sum_weight;
    
    public static float score(){        
        
        int weight;
        int scrore_earned;
        int scores_shifted;
        int shift_among = 0;
        int total_point;
        float weight_score;
        
        System.out.print("Weight (0-100)? ");
        weight = input.nextInt();
             
        System.out.print("Score earned ? ");
        scrore_earned = input.nextInt();
        
        System.out.print("Were scores shifted (1 = yes, 2=no)? ");
        scores_shifted = input.nextInt();
        switch(scores_shifted){
            case 1:
                System.out.print("Shift Amound ? ");
                shift_among  = input.nextInt();               
                break;
            case 2:
                shift_among = 0;
                break;           
        }
        
        total_point = scrore_earned+shift_among;
        System.out.println("Total point = "+total_point+"/100");
        
        weight_score = ((float)total_point/100)*(float)weight;
        System.out.println("Weighted score = "+
                (float)Math.round(weight_score*10)/10+"/"+weight);
        
        sum_weight+=weight;
        return weight_score;
        
    }
    public static float min_grade(float sum){
        
        float grade = 0;
        
        if(sum>=85){
            grade = 3;
        }else if(sum>=75){
            grade = 2;
        }else if(sum>=60){
            grade = (float) 0.7;
        }else{
            grade = 0;
        }
        return grade;
    }
      
    public static void Begin(){
        
        System.out.println("This program reads exam/homework scores and reports"
                + " your overall course grade");
    }
    public static float Midterm(){
        
        System.out.println("\nMidterm: ");
        return score();
    }
    
    public static float Finalterm(){
    
        System.out.println("\nFinalterm :");
        return score();
    }
    
    public static float Homework(){
        int weight;
        int num_of_asm;
        int sections;
        int section_point;
        
        List<Integer> score = new ArrayList<>();
        List<Integer> max = new ArrayList<>();
        
        int sum_score = 0;
        int sum_max;
        float weight_score;
        
        System.out.println("Homework:");
        
        System.out.print("Weight (0-100)? ");
        weight = input.nextInt();
        
        sum_weight+=weight;
        if(sum_weight!=100){
            
            weight=weight+(100-sum_weight);
        }
        
        System.out.print("Number of assignments ? ");
        num_of_asm = input.nextInt();
        
        for(int i =1;i<=num_of_asm;i++){
            System.out.print("Assignment "+i+" score and max");
            score.add(input.nextInt());
            max.add(input.nextInt());
        }
        System.out.print("How many sections did you attend ? ");
        sections = input.nextInt();
        
        section_point = sections*5;
        if (section_point>30){
        
            section_point = 30;
        }
        
        System.out.println("Section point = "+section_point);
               
        for(int i : score){
            
            sum_score+=i;
        }
        
        
        if(sum_score>150){
            
            sum_score = 150;
        }
        
        sum_score+=section_point;
        
        sum_max = 30;
        for(int i : max){
            sum_max +=i;
        }
        System.out.println("Total points = "+sum_score+"/"+sum_max);
        
        weight_score = (float)sum_score/(float)sum_max*weight;
        System.out.println("Weight score = "+
                (float)Math.round(weight_score*10)/10+"/"+weight);
        
        return weight_score;
    }
    
    public static void Report(){
        
        float sum = Midterm()+ Finalterm()+Homework();
        System.out.println("Overall percentage = "+ sum);
        
        System.out.println("Your grade will be at least" + min_grade(sum));
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        Begin();
        Report();
    }
    
}
